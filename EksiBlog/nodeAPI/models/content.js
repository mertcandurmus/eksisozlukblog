const mongoose = require('mongoose');

var Content = mongoose.model('Content', 
{
    title: { type:String },
    content: { type:String },
    comment: { type:String }
})

module.exports = { Content }