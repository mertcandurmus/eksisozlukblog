require('./config/config');                     //yeni
require('./db');      
require('./config/passportConfig');                 //yeni ekledim



const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const { mongoose } = require('./db.js');
const passport = require('passport')               //login



var ContentController = require('./controllers/contentController.js')

const rtsIndex = require('./routes/index.router');


var app = express()
app.use(bodyParser.json())
app.use('/content',ContentController)
app.use(cors())
app.options('*', cors());
app.use('/api', rtsIndex);


app.use(passport.initialize());                   //login

 
// error handler    //loginle birlikter ekkledim
app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors)
    }
});



 app.listen(3000, () => console.log('server started at 3000'))
