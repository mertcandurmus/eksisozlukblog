const express = require('express')

var ObjectId = require('mongoose').Types.ObjectId;

var router = express.Router();

var { Content } = require('../models/content')

//get metodu
// => localhost:3000/content/
router.get('/', (req, res) => {
    Content.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Content :' + JSON.stringify(err, undefined, 2)); }
    });
});

//get metodu byıd
router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Content.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Content :' + JSON.stringify(err, undefined, 2)); }
    });
});


//post metodu
router.post('/', (req, res) => {

    var cnt = new Content({
        title: req.body.title,
        content: req.body.content,
        comment: req.body.comment,
    });
    cnt.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Content Save :' + JSON.stringify(err, undefined, 2)); }
    });
});


//put metodu  update için
router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send('No record with given id : ${req.params.id}');

        var cnt = {
            title: req.body.title,
            content: req.body.content,
            comment: req.body.comment,
        };
    Content.findByIdAndUpdate(req.params.id, { $set: cnt }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Content Update :' + JSON.stringify(err, undefined, 2)); }
    });
});




//delete methodu
router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send('No record with given id : ${req.params.id}');

    Content.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Content Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});




module.exports = router

