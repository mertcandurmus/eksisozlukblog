import { Component, OnInit } from '@angular/core';
import { UserRegisterService } from 'src/app/user-register.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private userService: UserRegisterService, private router: Router) { }
  userDetails;

  ngOnInit() {
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
      },
      err => {
        console.log(err);
  }
    );
}


onLogout(){
  this.userService.deleteToken();
  this.router.navigate(['/login']);
}


onbaslikAc() {
   this.router.navigate(['/add-content']);
 
 }


}
