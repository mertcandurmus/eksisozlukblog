import { Component } from '@angular/core';

import { RegistrationService } from './registration.service';

//import { ContentComponent } from './src/app/content';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'frontEnd';
}
