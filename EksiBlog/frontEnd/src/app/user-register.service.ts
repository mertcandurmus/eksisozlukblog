import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserRegisterService {


  url2 = 'http://localhost:3000/api/register';
  url3 = 'http://localhost:3000/api/authenticate';
  url4 = 'http://localhost:3000/api/userProfile';

  // tslint:disable-next-line:object-literal-key-quotes
  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(private http2: HttpClient) { }

  registerUser(usersData) {
    return this.http2.post<any>(this.url2, usersData, this.noAuthHeader);
  }


  getUserProfile() {
    return this.http2.get(this.url4);
  }

  login(authCredentials){
    return this.http2.post<any>(this.url3, authCredentials, this.noAuthHeader);
  }


  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  deleteToken() {
    localStorage.removeItem('token');
  }

  getUserPayload() {
    var token = this.getToken();
    if (token) {
      var userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    } else{
      return null;
    }
  }

  isLoggedIn() {
    var userPayload = this.getUserPayload();
    if (userPayload)
      return userPayload.exp > Date.now() / 1000;
    else
      return false;
  }


}
