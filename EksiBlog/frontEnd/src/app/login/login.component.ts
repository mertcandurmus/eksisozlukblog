import { Component, OnInit } from '@angular/core';
import { UserRegisterService } from 'src/app/user-register.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from "@angular/router";



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: UserRegisterService, private router: Router) { }
  serverErrorMessages: string;


  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  }
);

  onSubmit(){
    console.log(this.loginForm.value);
    this.loginService.login(this.loginForm.value).subscribe(
      response => {
        console.log('Success!', response);
        // tslint:disable-next-line:no-string-literal
        this.loginService.setToken(response['token']);
       // tslint:disable-next-line:align
       this.router.navigateByUrl('/userProfile');
      },
      error => console.error('Error!', error)
    );
}
ngOnInit() {
}

}
