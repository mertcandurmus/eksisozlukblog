import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Content } from '@angular/compiler/src/render3/r3_ast';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

 contents: Content[];

  _url = 'http://localhost:3000/content';
  
  constructor(private _http: HttpClient) { }

  register(userData) {
    return this._http.post<any>(this._url, userData);
  }

  getContent(){
    return this._http.get(this._url);
  }
}
