import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TestComponent } from './test/test.component';

const routes: Routes = [
  {
    path: 'add-content',
    component: ContentComponent

  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'register',
    component: RegisterComponent

  },
  {
    path: 'userProfile',
    component: UserProfileComponent, canActivate: [AuthGuard]

  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'test',
    component: TestComponent

  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
