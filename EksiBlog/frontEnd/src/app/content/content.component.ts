import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { RegistrationService } from 'src/app/registration.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  registrationFrom = new FormGroup({ 
      title: new FormControl(''),
      content: new FormControl(''),
      comment: new FormControl('')
    }
  );

  onSubmit() {
    console.log(this.registrationFrom.value);
    this._registrationService.register(this.registrationFrom.value)
    .subscribe(
      response => console.log('Success!', response),
      error => console.error('Error!', error)
    );
  };
  constructor(private _registrationService: RegistrationService) { }

  ngOnInit() {
  } 
}
