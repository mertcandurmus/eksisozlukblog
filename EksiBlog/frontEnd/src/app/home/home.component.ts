import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/app/registration.service';
import { Content } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [RegistrationService]
})
export class HomeComponent implements OnInit {
 

  constructor(private registrationService: RegistrationService ) { }

  ngOnInit() {
    this.refreshContentList();
  }

  refreshContentList() {
    this.registrationService.getContent().subscribe((res) => {
      this.registrationService.contents = res as Content[];
    });
  }

}
