import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserRegisterService } from 'src/app/user-register.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup({ 
    fullName: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl('')
  }
);

  // tslint:disable-next-line:max-line-length
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  showSucessMessage: boolean;

  constructor(private registrationService2: UserRegisterService) { }

  ngOnInit() {
  }

  onSubmit(){
    console.log(this.registerForm.value);
    this.registrationService2.registerUser(this.registerForm.value)
    .subscribe(
      response => {
        console.log('Success!', response);
        this.showSucessMessage = true;
      },
      error => console.error('Error!', error)
    );
  };
  
}
